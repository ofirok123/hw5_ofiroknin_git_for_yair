#pragma once

#include "Shape.h"
#include "Menu.h"
#include "Point.h"

#define PI 3.14

class Circle : public Shape
{
private:
	Point _center;
	double _radius;

public:
	Circle(const Point& center, double radius, const std::string& type, const std::string& name);
	~Circle();

	const Point& getCenter() const;
	double getRadius() const;

	virtual double getArea() const override;
	virtual double getPerimeter() const override;
	virtual void draw(const Canvas& canvas) override;
	virtual void clearDraw(const Canvas& canvas) override;
	virtual void move(const Point& other) override;

	// override functions if need (virtual + pure virtual)


};