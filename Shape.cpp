#include "Shape.h"

Shape::Shape(const std::string& name, const std::string& type)
{
	this->_name = name;
	this->_type = type;
}

Shape::Shape()
{
	this->_name = "";
	this->_type = "";
}

void Shape::printDetails() const
{
	cout << "Shape name: " << this->_name << endl;
	cout << "Type name: " << this->_type << endl;
}

std::string Shape::getType() const
{
	return this->_type;
}

std::string Shape::getName() const
{
	return this->_name;
}