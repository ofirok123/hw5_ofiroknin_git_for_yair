#pragma once
#include "Polygon.h"
#include "Menu.h"


namespace myShapes
{
	// Calling it MyRectangle becuase Rectangle is taken by global namespace.
	class Rectangle : public Polygon
	{

	public:
		// There's a need only for the top left corner 
		Rectangle(const Point& a, double length, double width, const std::string& type, const std::string& name);
		virtual ~Rectangle();
		virtual double getArea() const override;
		virtual double getPerimeter() const override;
		virtual void draw(const Canvas& canvas) override;
		virtual void move(const Point& other) override; // add the Point to all the points of shape
		virtual void clearDraw(const Canvas& canvas) override;


		// override functions if need (virtual + pure virtual)

	};
}