#pragma once
#include "Shape.h"
#include "Canvas.h"
#include <vector>
#include <iostream>


using namespace std;
using std::string;
using std::cout;
using std::cin;
using std::endl;

const string CircleType = "Circle";
const string RectType = "Rectangle";
const string TriType = "Triangle";
const string ArrowType = "Arrow";

class Menu
{
public:

	Menu();
	~Menu();
	void printFirstMenu();
	void CircleMenu(double* x, double* y, double* radius);
	void ShapesMenu();


	// more functions..
	Canvas& getCanv();

private:


	Canvas _canvas;


	
};

