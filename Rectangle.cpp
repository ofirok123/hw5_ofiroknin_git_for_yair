#include "Rectangle.h"

using namespace myShapes;


Rectangle::Rectangle(const Point& a, double length, double width, const std::string& type, const std::string& name) : Polygon(name,type)
{
	if (length > 0 && width > 0)
	{
		this->_points[0] = Point(a.getX(), a.getY());
		this->_points[1] = Point((a.getX() + width), a.getY() - length);
	}
	else
	{
		std::cerr << "No negative values for Lenght or Width to rectangle" << endl;
		_exit(1);
	}
	
}

Rectangle::~Rectangle()
{

}

void myShapes::Rectangle::draw(const Canvas& canvas)
{
	canvas.draw_rectangle(_points[0], _points[1]);
}

void myShapes::Rectangle::clearDraw(const Canvas& canvas)
{
	canvas.clear_rectangle(_points[0], _points[1]);
}

double Rectangle::getArea() const
{
	double width = _points[1].getX() - _points[0].getX();
	double length = _points[1].getY() - _points[0].getY();
	return width * length;
}

double Rectangle::getPerimeter() const
{
	double width = _points[1].getX() - _points[0].getX();
	double length = _points[1].getY() - _points[0].getY();
	return (length * 2) + (width * 2);
}

void Rectangle::move(const Point& other)
{
	this->_points[0] += other;
	this->_points[1] += other;
}




