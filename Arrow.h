#pragma once
#include "Point.h"
#include "Shape.h"
#include "Polygon.h"


class Arrow : public Shape
{
public:
	Arrow(const Point& a, const Point& b, const std::string& type, const std::string& name);
	~Arrow();

	// override functions if need (virtual + pure virtual)
	virtual double getArea() const override;
	virtual double getPerimeter() const override;
	virtual void draw(const Canvas& canvas) override;
	virtual void move(const Point& other) override; // add the Point to all the points of shape
	virtual void clearDraw(const Canvas& canvas) override;

private:
	std::vector<Point> _points;
};