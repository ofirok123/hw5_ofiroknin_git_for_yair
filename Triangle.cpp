#include "Triangle.h"

Triangle::Triangle(const Point& a, const Point& b, const Point& c, const std::string& type, const std::string& name) : Polygon(name,type)
{
	if ((a.getX() == b.getX()) || a.getX() == c.getX() || (a.getY() == b.getY()) || (a.getY() == c.getY()))
	{
		std::cerr << "Triangle edges can't be on the same axis" << endl;
		_exit(1);
	}
	else
	{
		_points[0] = Point(a);
		_points[1] = Point(b);
		_points[2] = Point(c);
	}
}

Triangle::~Triangle()
{

}

void Triangle::draw(const Canvas& canvas)
{
	canvas.draw_triangle(_points[0], _points[1], _points[2]);
}

void Triangle::clearDraw(const Canvas& canvas)
{
	canvas.clear_triangle(_points[0], _points[1], _points[2]);
}

double Triangle::getArea() const
{
	double a = _points[0].getX() * (_points[1].getY() - _points[2].getY());
	double b = _points[1].getX() * (_points[2].getY() - _points[0].getY());
	double c = _points[2].getX() * (_points[0].getY() - _points[1].getY());
	return abs((a + b + c) / 2);
}

double Triangle::getPerimeter() const
{
	double a_b = _points[0].distance(_points[1]);
	double a_c = _points[0].distance(_points[2]);
	double b_c = _points[1].distance(_points[2]);
	return a_b + a_c + b_c;
}

void Triangle::move(const Point& other)
{
	_points[0] += other;
	_points[1] += other;
	_points[2] += other;


}

