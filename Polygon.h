#pragma once

#include "Shape.h"
#include "Menu.h"
#include "Point.h"
#include <vector>

using std::vector;

class Polygon : public Shape
{

public:
	Polygon(const std::string& type, const std::string& name);
	Polygon();
	virtual ~Polygon();

	// override functions if need (virtual + pure virtual)

protected:
	vector<Point> _points;
};