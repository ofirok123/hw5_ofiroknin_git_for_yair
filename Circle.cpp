#include "Circle.h"

void Circle::draw(const Canvas& canvas) // overrided
{
	canvas.draw_circle(getCenter(), getRadius());
}

void Circle::clearDraw(const Canvas& canvas) // overrided
{
	canvas.clear_circle(getCenter(), getRadius());
}

Circle::Circle(const Point& center, double radius, const std::string& type, const std::string& name) : Shape(name,type)
{
	if (radius <= 0)
	{
		std::cerr << "No negative radius value" << endl;
		_exit(1);
	}
	else
	{
		this->_center = center;
		this->_radius = radius;
	}
	
}

Circle::~Circle() 
{

}

const Point& Circle::getCenter() const
{
	return this->_center;
}

double Circle::getRadius() const
{
	return this->_radius;
}
 
double Circle::getArea() const // overrided
{
	return pow(this->_radius,2) * PI;
}

double Circle::getPerimeter() const // overrided
{
	return this->_radius * 2 * PI;
}

void Circle::move(const Point& other)
{
	this->_center += other;
}
